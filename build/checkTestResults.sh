 #!/bin/bash

#output results
TESTRUNID=$(cat testresults.json | jq '.summary.testRunId' -r)
sfdx force:apex:test:report -i $TESTRUNID

#assert status
TESTSTATUS=$(cat testresults.json | jq '.summary.outcome' -r)
if [ "$TESTSTATUS" == "Failed" ]; then
    exit 1;
else
    exit 0;
fi